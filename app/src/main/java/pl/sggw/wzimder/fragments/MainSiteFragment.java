package pl.sggw.wzimder.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pl.sggw.wzimder.FirstActivity;
import pl.sggw.wzimder.R;
import pl.sggw.wzimder.models.User;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

import static pl.sggw.wzimder.fragments.StatusEnum.DONTLIKE;
import static pl.sggw.wzimder.fragments.StatusEnum.LIKE;

/**
 * Created by mkowal on 12/27/15.
 */
public class MainSiteFragment extends Fragment {

    private ArrayList<User> al;
    private BaseAdapter arrayAdapter;
    private String authToken;

    @InjectView(R.id.frame)
    SwipeFlingAdapterView flingContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ((FirstActivity) getActivity()).setActionBarTitle(R.string.toolbar_menu_main_site);

        SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
        authToken = loginPreferences.getString("accessToken", "");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final BackendRepository backendRepository = new BackendRepository();

        View mainSiteFragmentView = inflater.inflate(R.layout.content_main_site, container, false);

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mainSiteFragmentView.getContext());
        alertBuilder.setCancelable(true);

        ButterKnife.inject(this, mainSiteFragmentView);

        al = new ArrayList<>();
        al.addAll(getUserList());

        arrayAdapter = new CustomCardAdapter(mainSiteFragmentView.getContext(), al);

        flingContainer.setAdapter(arrayAdapter);
        flingContainer.setMinStackInAdapter(2);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {

            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                al.remove(0);
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                User user = (User) dataObject;

                if (user.getId().equals("0")) return;

                Map<String, String> mapStatus = new HashMap<>();
                mapStatus.put("UserId", user.getId());
                mapStatus.put("Status", String.valueOf(DONTLIKE.getCode()));

                JSONObject jsonObject = new JSONObject(mapStatus);
                String requestJson = jsonObject.toString();
                String response = null;
                try {
                    response = backendRepository.makePostRequestWithToken(RequestType.SET_STATUS_REQUEST, requestJson, authToken);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                Log.d(response, "dontlike");

            }

            @Override
            public void onRightCardExit(Object dataObject) {
                User user = (User) dataObject;

                if (user.getId().equals("0")) return;

                Map<String, String> mapStatus = new HashMap<>();
                mapStatus.put("UserId", user.getId());
                mapStatus.put("Status", String.valueOf(LIKE.getCode()));

                JSONObject jsonObject = new JSONObject(mapStatus);
                String requestJson = jsonObject.toString();
                String response = null;
                try {
                    response = backendRepository.makePostRequestWithToken(RequestType.SET_STATUS_REQUEST, requestJson, authToken);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

                Log.d(response, "like");

            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                List<User> newUserList = new ArrayList<>();

                try {
                    newUserList = getUserList();
                } catch (NullPointerException e) {
                    newUserList.add(new User("0", "brak", "brak", 0));
                }
                if (newUserList.size() <= 1) {
                    newUserList.add(new User("0", "brak", "brak", 0));
                }
                al.addAll(newUserList);

                arrayAdapter.notifyDataSetChanged();
                Log.d("LIST", "notified");
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }

        });

        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                User user = (User) dataObject;
                user.getId();
                JSONObject userInfo = null;

                try {
                    userInfo = backendRepository.makeGetRequestWithId(RequestType.USER_INFO, user.getId(), authToken);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                String userInfoMsg = getUserInfoMsg(userInfo, user);
                alertBuilder.setMessage(userInfoMsg);
                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();

            }

            @SuppressWarnings("StringBufferMayBeStringBuilder")
            private String getUserInfoMsg(JSONObject userInfo, User user) {
                StringBuffer str = new StringBuffer("");
                try {
                    str.append(user.getFirstName()).append(" ");
                    str.append(userInfo.getString("LastName")).append(", ");
                    str.append(CustomCardAdapter.getUserAge(userInfo.getString("Birthdate"))).append("\n");
                    str.append("Opis: ").append(userInfo.getString("Description"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return str.toString();
            }
        });
        return mainSiteFragmentView;

    }

    @SuppressWarnings("unused")
    @OnClick(R.id.right)
    public void right() {
        /**
         * Trigger the right event manually.
         */
        flingContainer.getTopCardListener().selectRight();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.left)
    public void left() {
        flingContainer.getTopCardListener().selectLeft();
    }


    public static Fragment newInstance() {
        return new MainSiteFragment();
    }

    public JSONArray getUsers() throws IOException, JSONException {
        SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
        String authToken = loginPreferences.getString("accessToken", "");
        BackendRepository backendRepository = new BackendRepository();
        return backendRepository.makeGetRequestWithJsonArray(RequestType.GET_USERS, authToken);
    }

    public List<User> getUserList() {
        JSONArray responseJson = null;
        try {
            responseJson = getUsers();
            System.out.println(responseJson);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        ObjectMapper mapper = new ObjectMapper();
        assert responseJson != null;
        List<User> userList = null;
        String jsonString = responseJson.toString();
        try {
            userList = mapper.readValue(jsonString, new TypeReference<ArrayList<User>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        //noinspection ConstantConditions
        if (userList.isEmpty()) {
            al.add(new User("0", "brak", "brak", 0));
        }

        return userList;
    }
}
