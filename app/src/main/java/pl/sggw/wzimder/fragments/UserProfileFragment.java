package pl.sggw.wzimder.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import pl.sggw.wzimder.FirstActivity;
import pl.sggw.wzimder.R;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

/**
 * Created by mateusz on 12/29/15.
 */
public class UserProfileFragment extends Fragment {

    private View myUserProfileFragmentView;
    private final BackendRepository backendRepository = new BackendRepository();
    public int profilePhotoId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((FirstActivity) getActivity()).setActionBarTitle(R.string.toolbar_menu_your_profile);

        myUserProfileFragmentView = inflater.inflate(R.layout.content_user_profile, container, false);
        System.out.println("***************** myUserProfileFragmentView");
        //edycja UserProfileFragmentView
        try {
            setUserData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return myUserProfileFragmentView;
    }

    public static Fragment newInstance() {
        return new UserProfileFragment();
    }

    public void setUserData() throws JSONException {
            TextView chosen_nameToMessage = (TextView) myUserProfileFragmentView.findViewById(R.id.nameView2);
            TextView chosen_lastnameToMessage = (TextView) myUserProfileFragmentView.findViewById(R.id.lastNameView2);
            TextView chosen_birthdayToMessage = (TextView) myUserProfileFragmentView.findViewById(R.id.ageView2);
            TextView chosen_genderToMessage = (TextView) myUserProfileFragmentView.findViewById(R.id.genderView2);
            TextView chosen_descriptionToMessage = (TextView) myUserProfileFragmentView.findViewById(R.id.interestetView2);
            ImageView chosen_imageToMessage = (ImageView) myUserProfileFragmentView.findViewById(R.id.imageView2);

            JSONObject responseJsonObject = new JSONObject();

            try {
                SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
                String token = loginPreferences.getString("accessToken", "");
                responseJsonObject = backendRepository.makeGetRequest(RequestType.PROFILE_REQUEST, token);
                if (((FirstActivity) getActivity()).redirectFromPairs && ((FirstActivity) getActivity()).redirectIdParam != null) {
                    ((FirstActivity) getActivity()).setActionBarTitle(R.string.toolbar_menu_user_profile);
                    Button editButton = (Button) myUserProfileFragmentView.findViewById(R.id.button);
                    Button chatButton = (Button) myUserProfileFragmentView.findViewById(R.id.button2);
                    editButton.setOnClickListener(((FirstActivity) getActivity()));
                    editButton.setText(R.string.profile_button_backToPairs);
                    chatButton.setVisibility(View.VISIBLE);
                    System.out.println("RedirectId " + ((FirstActivity) getActivity()).redirectIdParam);
                    responseJsonObject = backendRepository.makeGetRequestWithId(RequestType.USER_INFO, ((FirstActivity) getActivity()).redirectIdParam, token);
                }
                System.out.println(responseJsonObject);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String firstName = responseJsonObject.get("FirstName").toString();
            String lastName = responseJsonObject.get("LastName").toString();
            String birthDate = responseJsonObject.getString("Birthdate");
            birthDate = birthDate.substring(0, birthDate.indexOf("T"));

            if (responseJsonObject.has("Id")) {
                String id = responseJsonObject.get("Id").toString();
                ((FirstActivity) getActivity()).userId = id;
            }

            String genderNumber = responseJsonObject.get("Gender").toString();
            String description = responseJsonObject.get("Description").toString();
            if (((FirstActivity) getActivity()).redirectFromPairs && ((FirstActivity) getActivity()).redirectIdParam != null) {
                profilePhotoId = Integer.parseInt(responseJsonObject.get("Photo").toString());
                ((FirstActivity) getActivity()).redirectFromPairs = false;
            } else {
                profilePhotoId = Integer.parseInt(responseJsonObject.get("ProfilePhotoId").toString());
            }

            String gender;
            switch (genderNumber) {
                case "1":
                    gender = "kobieta";
                    break;
                case "0":
                    gender = "mężczyzna";
                    break;
                default:
                    gender = "nie ustawione";
            }

            chosen_nameToMessage.setText(firstName);
            chosen_lastnameToMessage.setText(lastName);
            chosen_birthdayToMessage.setText(birthDate);
            chosen_genderToMessage.setText(gender);
            chosen_descriptionToMessage.setText(description);

            if (profilePhotoId != 0) {
                chosen_imageToMessage.setImageDrawable(backendRepository.getPhoto(profilePhotoId));
            }
            //wywołanie metody get do pobrania zdjęcia
    }


}
