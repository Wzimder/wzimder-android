package pl.sggw.wzimder.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import pl.sggw.wzimder.LoginActivity;
import pl.sggw.wzimder.MainActivity;
import pl.sggw.wzimder.R;


/**
 * Created by mkowal on 12/27/15.
 */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainToolbarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainToolbarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainToolbarFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public MainToolbarFragment() {
        // Required empty public constructor
    }

    public static MainToolbarFragment newInstance() {
        return new MainToolbarFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_toolbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.toolbar_fragment_main_site:
                replaceFragment(R.id.fragment_container, MainSiteFragment.newInstance());
                break;
            case R.id.toolbar_fragment_menu_search:
                replaceFragment(R.id.fragment_container, SearchEngineFragment.newInstance());
                break;
            //case R.id.toolbar_fragment_menu_chat:
//                replaceFragment(R.id.fragment_container, ChatFragment.newInstance());
 //               break;
            case R.id.toolbar_fragment_your_pairs:
                replaceFragment(R.id.fragment_container, UserPairsFragment.newInstance());
                break;
            case R.id.toolbar_fragment_your_profile:
                replaceFragment(R.id.fragment_container, UserProfileFragment.newInstance());
                break;
            case R.id.toolbar_fragment_logout:
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.toolbar_fragment_close:
                Intent intentex = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                intentex.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentex.putExtra("EXIT", true);
                startActivity(intentex);
                getActivity().finishAffinity();
                getActivity().finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean replaceFragment(int id, Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.commit();
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_toolbar, container, false);
        setHasOptionsMenu(true);

        replaceFragment(R.id.fragment_container, MainSiteFragment.newInstance());
        return view;
    }

    // Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
