package pl.sggw.wzimder.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.sggw.wzimder.FirstActivity;
import pl.sggw.wzimder.R;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

public class ChatFragment extends Fragment {

    private final BackendRepository backendRepository = new BackendRepository();

    private View chatFragmentView;
    private ScrollView scrollView;
    private EditText newMessage;
    private TableLayout tableLayout;
    private TableRow tr;
    private String userId;
    private Handler h;
    private Runnable runnable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((FirstActivity) getActivity()).setActionBarTitle(R.string.toolbar_menu_chat);

        Bundle args = getArguments();
        userId = args.getString("userId");

        chatFragmentView = inflater.inflate(R.layout.content_chat, container, false);

        scrollView = (ScrollView)chatFragmentView.findViewById(R.id.scrollView);
        tableLayout = (TableLayout)chatFragmentView.findViewById(R.id.tableLayout);

        Button sendMessageBtn = (Button)chatFragmentView.findViewById(R.id.sendMessageButton);
        newMessage = (EditText)chatFragmentView.findViewById(R.id.newMessage);



        h = new Handler();
        final int delay = 8000; //milliseconds
        runnable = new Runnable(){
                    public void run(){
                        try {
                            getNewMessages();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        h.postDelayed(this, delay);
                    }
                };

        h.postDelayed(runnable, delay);

        scrollDown();


        sendMessageBtn.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View v) {
                String msg = newMessage.getText().toString();
                newMessage.setText("");

                if(!msg.equals("") && msg.trim().length() > 0) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("Text", msg);
                        object.put("RecipientId", userId);
                        String objString = object.toString();

                        addMessage(tableLayout, object);

                        SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
                        String token = loginPreferences.getString("accessToken", "");
                        String result = backendRepository.makePostRequestWithToken(RequestType.POST_MESSAGE, objString, token);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        try {
            getUser();
            getNewMessages();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return chatFragmentView;
    }

    @Override
    public void onStop() {
        super.onStop();
        h.removeCallbacks(runnable);
    }

    public static Fragment newInstance() {
        return new ChatFragment();
    }


    public void getUser() throws JSONException {
        JSONObject responseJsonObject = new JSONObject();

        try {
            SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
            String token = loginPreferences.getString("accessToken", "");
            responseJsonObject = backendRepository.makeGetRequestWithId(RequestType.USER_INFO, userId, token);

            TextView userName = (TextView)chatFragmentView.findViewById(R.id.userName);
            ImageView image = (ImageView) chatFragmentView.findViewById(R.id.imageView);

            userName.setText(responseJsonObject.getString("FirstName") + " " + responseJsonObject.getString("LastName"));
            int photoId = Integer.parseInt(responseJsonObject.getString("Photo"));
            if (photoId != 0) {
                image.setImageDrawable(backendRepository.getPhoto(photoId));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void getNewMessages() throws JSONException {
        JSONArray responseJsonArray = new JSONArray();

        try {
            SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
            String token = loginPreferences.getString("accessToken", "");
            responseJsonArray = backendRepository.makeGetRequestWithIdAsJsonArray(RequestType.GET_MESSAGES, userId, token);
            addMessages(tableLayout, responseJsonArray);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addMessage(TableLayout tableLayout, JSONObject message) throws JSONException
    {
        tr = new TableRow(chatFragmentView.getContext());

        addTextViews(tr, message);
        tableLayout.addView(tr, tableLayout.getChildCount());

        scrollDown();

    }

    private void addMessages(TableLayout tableLayout, JSONArray messages) throws JSONException
    {
        for(int i=0; i < messages.length(); i++)
        {
            tr = new TableRow(chatFragmentView.getContext());

            addTextViews(tr, messages.getJSONObject(i));
            tableLayout.addView(tr, tableLayout.getChildCount());
        }

        scrollDown();
    }

    private void addTextViews(TableRow tr, JSONObject json) throws JSONException {
        if(json.has("SenderId"))
            tr.addView(getTextView(json.getString("Text"), new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f)), 0);
        else
            tr.addView(getTextViewMy(json.getString("Text"), new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f)), 0);

        scrollDown();

    }


    private TextView getTextView(String text, ViewGroup.LayoutParams layoutParams) {
        TextView tv = new TextView(chatFragmentView.getContext());
        tv.setTextSize(22);
        tv.setText(text);
        tv.setWidth(0);
        tv.setPadding(0, 10, 0, 10);
        tv.setLayoutParams(layoutParams);
        return tv;
    }

    private TextView getTextViewMy(String text, ViewGroup.LayoutParams layoutParams) {
        TextView tv = new TextView(chatFragmentView.getContext());
        tv.setTextSize(22);
        tv.setText(text);
        tv.setWidth(0);
        tv.setPadding(0, 10, 0, 10);
        tv.setLayoutParams(layoutParams);
        tv.setGravity(Gravity.RIGHT);
        return tv;
    }

    private void scrollDown() {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }
}
