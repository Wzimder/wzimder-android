package pl.sggw.wzimder.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Years;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.sggw.wzimder.FirstActivity;
import pl.sggw.wzimder.R;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

import static java.lang.Integer.*;

/**
 * Created by monika1212d
 */
public class UserPairsFragment extends Fragment {
    private final BackendRepository backendRepository = new BackendRepository();
    private View myPairsFragmentView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((FirstActivity) getActivity()).setActionBarTitle(R.string.toolbar_menu_your_pairs);
        myPairsFragmentView = inflater.inflate(R.layout.content_user_pairs, container, false);
        ListView listView = (ListView) myPairsFragmentView.findViewById(R.id.listView);
        List<List<responsePerson>> dataSource = new ArrayList<List<responsePerson>>();
        JSONArray responseJsonObjects = new JSONArray();
        try {
            SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
            String token = loginPreferences.getString("accessToken", "");
            responseJsonObjects = backendRepository.makeGetRequestAsJsonArray(RequestType.ALL_PAIRS_REQUEST, token);
            System.out.println(responseJsonObjects);
            List<responsePerson> tmpList = new ArrayList<>();
            for (int i = 0; i < responseJsonObjects.length(); i++) {
                String id = responseJsonObjects.getJSONObject(i).getString("Id");
                String photoId = responseJsonObjects.getJSONObject(i).getString("Photo");
                String firstName = responseJsonObjects.getJSONObject(i).getString("FirstName");
                String lastName = responseJsonObjects.getJSONObject(i).getString("LastName");
                String birthdate = responseJsonObjects.getJSONObject(i).getString("Birthdate");
                responsePerson rp = new responsePerson();
                rp.id = id;
                rp.firstName = firstName;
                rp.lastName = lastName;
                rp.setAge(birthdate);
                rp.setPhoto(parseInt(photoId));
                if (i % 2 == 0 && i + 1 != responseJsonObjects.length()) {
                    tmpList.add(0, rp);
                }

                if (i % 2 == 0 && i + 1 == responseJsonObjects.length()) {
                    tmpList.add(0, rp);
                    tmpList.add(1, null);
                    dataSource.add(tmpList);
                    tmpList = new ArrayList<>();
                }
                if (i % 2 != 0) {
                    tmpList.add(1, rp);
                    dataSource.add(tmpList);
                    tmpList = new ArrayList<>();
                }
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        PairsAdapter adapter = new PairsAdapter(this.getActivity(), R.layout.content_user_pairs_row, dataSource, ((FirstActivity) getActivity()).mappings);
        System.out.println();
        listView.setAdapter(adapter);
        return myPairsFragmentView;
    }

    public static Fragment newInstance() {
        return new UserPairsFragment();
    }

    //simple person class
    private class responsePerson {
        public String id;
        public String firstName;
        public String lastName;
        public Drawable photo;
        public int age;

        public void setAge(String birthdate) {
            DateTime dateTimeBirthdate = DateTime.parse(birthdate);
            this.age = Years.yearsBetween(dateTimeBirthdate, new DateTime()).getYears();
        }

        public void setPhoto(Integer photoId) {
            if (photoId != 0)
                this.photo = backendRepository.getPhoto(photoId);
            else
                this.photo = getResources().getDrawable(R.mipmap.brakzdjecia);
        }
    }

    // adapter for pairs
    private static class PairsAdapter extends BaseAdapter {
        private Activity parentActivity;
        private int itemLayoutId;
        private List<List<responsePerson>> dataSource;
        private LayoutInflater inflater;
        private Map<Integer, String> mappings;

        public PairsAdapter(Activity activity, int layoutId, List<List<responsePerson>> ds, Map<Integer, String> mappings) {
            parentActivity = activity;
            itemLayoutId = layoutId;
            dataSource = ds;
            this.mappings = mappings;
            inflater = (LayoutInflater) this.parentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parentView) {
            Display display = ((FirstActivity)parentActivity).getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            int width = valueOf(String.valueOf((size.x / 2)-10));
            View view = null;
            if (convertView == null) {
                view = inflater.inflate(itemLayoutId, parentView, false);
                //image box 1
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
                imageView.getLayoutParams().height = width;
                imageView.getLayoutParams().width = width;
                Drawable photo = dataSource.get(pos).get(0).photo;
                imageView.setBackground(photo);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.requestLayout();
                //user mapping
                imageView.setTag(new Integer(++((FirstActivity)parentActivity).tagStatus));
                mappings.put((Integer)imageView.getTag(), dataSource.get(pos).get(0).id);
                //textview 1
                TextView textView = (TextView) view.findViewById(R.id.textView2);
                String data = dataSource.get(pos).get(0).firstName + " " + dataSource.get(pos).get(0).lastName + ", " + dataSource.get(pos).get(0).age;
                textView.setText(data);
                if (dataSource.get(pos).get(1) != null) {
                    //image box 2
                    ImageView imageView2 = (ImageView) view.findViewById(R.id.imageView3);
                    imageView2.getLayoutParams().height = width;
                    imageView2.getLayoutParams().width = width;
                    Drawable photo2 = dataSource.get(pos).get(1).photo;
                    imageView2.setBackground(photo2);
                    imageView2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView2.requestLayout();
                    //user mapping
                    imageView2.setTag(new Integer(++((FirstActivity)parentActivity).tagStatus));
                    mappings.put((Integer) imageView2.getTag(), dataSource.get(pos).get(1).id);
                    //textview 2
                    TextView textView2 = (TextView) view.findViewById(R.id.textView3);
                    String data2 = dataSource.get(pos).get(1).firstName + " " + dataSource.get(pos).get(1).lastName + ", " + dataSource.get(pos).get(1).age;
                    textView2.setText(data2);
                } else {
                    ImageView imageView2 = (ImageView) view.findViewById(R.id.imageView3);
                    TextView textView2 = (TextView) view.findViewById(R.id.textView3);
                    imageView2.setVisibility(View.GONE);
                    textView2.setVisibility(View.GONE);
                }
            } else {
                view = convertView;
            }
            return view;
        }


        @Override
        public int getCount() {
            return (dataSource != null) ? dataSource.size() : 0;
        }

        @Override
        public Object getItem(int idx) {
            return (dataSource != null) ? dataSource.get(idx) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public int getItemViewType(int pos) {
            return IGNORE_ITEM_VIEW_TYPE;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }
    }

}


