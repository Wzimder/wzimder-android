package pl.sggw.wzimder.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Years;

import java.util.ArrayList;

import pl.sggw.wzimder.R;
import pl.sggw.wzimder.models.User;
import pl.sggw.wzimder.resource.BackendRepository;

/**
 * Created by mateusz on 1/6/16.
 */
public class CustomCardAdapter extends BaseAdapter {

    private ArrayList<User> al;
    private Context context;

    public CustomCardAdapter(Context context, ArrayList<User> al) {
        this.context = context;
        this.al = al;
    }

    @Override
    public int getCount() {
        return al.size();
    }

    @Override
    public User getItem(int position) {
        return al.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.valueOf(al.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User user = getItem(position);
        BackendRepository backendRepository = new BackendRepository();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.main_site_item, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.card_photo);
        TextView textView = (TextView) convertView.findViewById(R.id.item_desc_under_photo);

        if (user.getPhotoId() != 0) {
            imageView.setImageDrawable(backendRepository.getPhoto(user.getPhotoId()));
        }

        textView.setText(getUserDesc(user));

        return convertView;

    }

    private String getUserDesc(User user) {
        return user.getFirstName() + ", " + getUserAge(user.getBirthdate());
    }

    protected static int getUserAge(String birthdate) {
        if (birthdate.equals("brak")) {
            return 0;
        }
        DateTime dateTimeBirthdate = DateTime.parse(birthdate);
        return Years.yearsBetween(dateTimeBirthdate, new DateTime()).getYears();
    }
}
