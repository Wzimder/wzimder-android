package pl.sggw.wzimder.fragments;

/**
 * Created by mateusz on 1/6/16.
 */
public enum StatusEnum {
    LIKE(0),
    DONTLIKE(1),
    UNKNOWN(2);

    private final int code;

    StatusEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
