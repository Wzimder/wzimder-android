package pl.sggw.wzimder.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import pl.sggw.wzimder.FirstActivity;
import pl.sggw.wzimder.R;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

/**
 * Created by mkowal on 12/27/15.
 */
@SuppressWarnings("FieldCanBeLocal")
public class SearchEngineFragment extends Fragment {

    public static String json;

    private View mySearchEngineFragmentView;

    private final BackendRepository backendRepository = new BackendRepository();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((FirstActivity) getActivity()).setActionBarTitle(R.string.toolbar_menu_search);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mySearchEngineFragmentView = inflater.inflate(R.layout.content_search_engine, container, false);

        //Read value from "getJson" and assign it to default view

        try {
            setDefaultSpinner();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button button = (Button) mySearchEngineFragmentView.findViewById(R.id.filtrButton);

        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String json = getJSONdata();
                            System.out.println(json);

                            SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
                            String authToken = loginPreferences.getString("accessToken", "");

                            String response = backendRepository.makePutRequest(RequestType.SEARCH_REQUEST, json, authToken);
                            System.out.println(response);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        replaceFragment(R.id.fragment_container, MainSiteFragment.newInstance());

                    }
                }
        );

        return mySearchEngineFragmentView;
    }

    public static Fragment newInstance() {
        return new SearchEngineFragment();
    }

    //post infrormaction to backend
    public String getJSONdata() throws JSONException {
        JSONObject json = new JSONObject();
        String chosen_genderToMessage = ((Spinner) getActivity().findViewById(R.id.genderlist)).getSelectedItem().toString();
        int chosen_fromAgeToMessage = Integer.parseInt(((Spinner) getActivity().findViewById(R.id.fromAgelist)).getSelectedItem().toString());
        int chosen_toAgeToMessage = Integer.parseInt(((Spinner) getActivity().findViewById(R.id.toAgelist)).getSelectedItem().toString());
        int chosen_distanceToMessage = Integer.parseInt(((Spinner) getActivity().findViewById(R.id.distanceList)).getSelectedItem().toString());

        int search_genderToMessage = 2;

        switch (chosen_genderToMessage) {
            case "kobieta":
                search_genderToMessage = 1;
                break;
            case "mężczyzna":
                search_genderToMessage = 0;
                break;
            default:
                search_genderToMessage = 2;

        }
        json.put("MinAge", chosen_fromAgeToMessage);
        json.put("MaxAge", chosen_toAgeToMessage);
        json.put("Gender", search_genderToMessage);
        json.put("MaxDistanceKM", chosen_distanceToMessage);

        return json.toString();
    }

    private boolean replaceFragment(int id, Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.commit();
        return true;
    }

    public void setDefaultSpinner() throws JSONException {
        Spinner spinnerFromAge = (Spinner) mySearchEngineFragmentView.findViewById(R.id.fromAgelist);
        Spinner spinnerToAge = (Spinner) mySearchEngineFragmentView.findViewById(R.id.toAgelist);
        Spinner spinnerGender = (Spinner) mySearchEngineFragmentView.findViewById(R.id.genderlist);
        Spinner spinnerDistance = (Spinner) mySearchEngineFragmentView.findViewById(R.id.distanceList);
        JSONObject responseJsonObject = new JSONObject();
        try {
            SharedPreferences loginPreferences = getActivity().getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
            String authToken = loginPreferences.getString("accessToken", "");
            responseJsonObject = backendRepository.makeGetRequest(RequestType.SEARCH_REQUEST, authToken);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int minAge = Integer.parseInt(responseJsonObject.get("MinAge").toString());
        int maxAge = Integer.parseInt(responseJsonObject.get("MaxAge").toString());
        int gender = Integer.parseInt(responseJsonObject.get("Gender").toString());
        int maxDistanceKM = Integer.parseInt(responseJsonObject.get("MaxDistanceKM").toString());

        int defaultMinAge = 0;

        switch (minAge) {
            case 18:
                defaultMinAge = 0;
                break;
            case 20:
                defaultMinAge = 1;
                break;
            case 25:
                defaultMinAge = 2;
                break;
            case 30:
                defaultMinAge = 3;
                break;
            case 35:
                defaultMinAge = 4;
                break;
            case 40:
                defaultMinAge = 5;
                break;
            case 50:
                defaultMinAge = 6;
                break;
            case 60:
                defaultMinAge = 7;
                break;
            default:
                defaultMinAge = 0;
        }

        spinnerFromAge.setSelection(defaultMinAge);

        int defaultMaxAge = 0;
        switch (maxAge) {
            case 20:
                defaultMaxAge = 0;
                break;
            case 25:
                defaultMaxAge = 1;
                break;
            case 30:
                defaultMaxAge = 2;
                break;
            case 35:
                defaultMaxAge = 3;
                break;
            case 40:
                defaultMaxAge = 4;
                break;
            case 50:
                defaultMaxAge = 5;
                break;
            case 60:
                defaultMaxAge = 6;
                break;
            case 70:
                defaultMaxAge = 7;
                break;
            case 90:
                defaultMaxAge = 8;
                break;
            default:
                defaultMaxAge = 0;
        }
        spinnerToAge.setSelection(defaultMaxAge);


        int defaultGender = 0;
        switch (gender) {
            case 0:
                defaultGender = 1;
                break;
            case 1:
                defaultGender = 0;
                break;
            default:
                defaultGender = 0;
        }
        spinnerGender.setSelection(defaultGender);

        int defaultMaxDistance = 0;
        switch (maxDistanceKM) {
            case 50:
                defaultMaxDistance = 0;
                break;
            case 100:
                defaultMaxDistance = 1;
                break;
            case 150:
                defaultMaxDistance = 2;
                break;
            case 200:
                defaultMaxDistance = 3;
                break;
            default:
                defaultMaxDistance = 0;
        }
        spinnerDistance.setSelection(defaultMaxDistance);
    }

}
