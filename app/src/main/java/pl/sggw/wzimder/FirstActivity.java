package pl.sggw.wzimder;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import pl.sggw.wzimder.fragments.ChatFragment;
import pl.sggw.wzimder.fragments.MainToolbarFragment.OnFragmentInteractionListener;
import pl.sggw.wzimder.fragments.UserPairsFragment;
import pl.sggw.wzimder.fragments.UserProfileFragment;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

/**
 * Created by mkowal on 11/28/15.
 */
public class FirstActivity extends AppCompatActivity implements OnFragmentInteractionListener, View.OnClickListener{

    private LocationManager locationManager;
    private LocationListener locationListener;
    public Map<Integer, String> mappings = new HashMap<>();
    public Boolean redirectFromPairs = false;
    public String redirectIdParam = null;
    public String userId = "";
    public Integer tagStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_fragment_main);
        setSupportActionBar(toolbar);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                System.out.println("Location changed: " +
                                location.getLatitude() + location.getLongitude()
                );
                postLocationToBackend(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                System.out.println("Status changed!");
            }

            @Override
            public void onProviderEnabled(String provider) {
                System.out.println("Provider enabled!");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET
                }, 10);
            }
            setRequestLocationUpdates();
        } else {
            setRequestLocationUpdates();
        }

    }

    private void postLocationToBackend(Location location) {
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();

        DecimalFormat backendDecimalFormat = new DecimalFormat("00.000000");
        String formatedLongitude = backendDecimalFormat.format(longitude);
        String formatedLatitude = backendDecimalFormat.format(latitude);

        System.out.println(formatedLatitude + " " + formatedLongitude);

        Map<String, String> mapLocation = new HashMap<>();
        mapLocation.put("Location", formatedLatitude + ";" + formatedLongitude);
        JSONObject jsonLocation = new JSONObject(mapLocation);

        BackendRepository backendRepository = new BackendRepository();

        SharedPreferences loginPreferences;
        try {
            loginPreferences = getSharedPreferences("loginPreferences", Activity.MODE_PRIVATE);
        } catch (NullPointerException ex) {
            return;
        }

        String authToken = loginPreferences.getString("accessToken", "");

        String response = null;
        try {
            response = backendRepository.makePostRequestWithToken(RequestType.SET_LOCATION, jsonLocation.toString(), authToken);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        System.out.println(response);
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setRequestLocationUpdates();
                }
        }
    }

    @SuppressWarnings("ResourceType")
    private void setRequestLocationUpdates() {
        System.out.println("setRequestLocationUpdates()");
        String locationProvider = LocationManager.GPS_PROVIDER;
        locationManager.requestLocationUpdates(locationProvider, 600000, 10000, locationListener);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    public void showPairedUserDetails(View view) {
        redirectFromPairs = true;
        redirectIdParam = mappings.get((Integer)view.getTag());
        replaceFragment(R.id.fragment_container, UserProfileFragment.newInstance());
    }

    public void chatWithUser(View view) {
        Fragment chatFragment = ChatFragment.newInstance();
        Bundle args = new Bundle();
        args.putString("userId", userId);
        chatFragment.setArguments(args);
        replaceFragment(R.id.fragment_container, chatFragment);
    }

    private boolean replaceFragment(int id, Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.commit();
        return true;
    }

    public void setActionBarTitle(int title) {
        setTitle(title);
    }

    public void onClick(View v) {
        replaceFragment(R.id.fragment_container, UserPairsFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        /*mkowal: User must logout or quit app by button*/
    }
}
