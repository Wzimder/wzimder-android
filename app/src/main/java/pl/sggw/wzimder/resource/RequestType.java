package pl.sggw.wzimder.resource;

/**
 * Created by mkowal on 12/21/15.
 */
public enum RequestType {
    REGISTER,
    LOGIN,
    SEARCH_REQUEST,
    PROFILE_REQUEST,
    GET_USERS,
    SET_LOCATION,
    SET_STATUS_REQUEST,
    USER_INFO,
    PROFILE_IMAGR_REQUEST,
    ALL_PAIRS_REQUEST,
    GET_MESSAGES,
    POST_MESSAGE
}
