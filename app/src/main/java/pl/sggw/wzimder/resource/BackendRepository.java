package pl.sggw.wzimder.resource;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mkowal on 12/21/15.
 */
@SuppressWarnings("FieldCanBeLocal")
public class BackendRepository {

    private final static String CONTENT_TYPE_JSON = "application/json";
    private final static String CONTENT_TYPE_FORM_URLENCODED = "application/x-www-form-urlencoded";
    private final static String BASE = "http://52.30.121.166/Backend/api";
    private final static String REG_ADDR = BASE + "/account/register";
    private final static String LOGIN_ADDR = BASE + "/account/login";
    private final static String SEARCH_ADDR = BASE + "/searchoptions";
    private final static String PROFIL_ADDR = BASE + "/account";
    private final static String PAIRS_ADDR = BASE + "/pairs";
    private final static String LOCATION = BASE + "/account/location";
    private final static String USERS_ADDR = BASE + "/users";
    private final static String STATUS_OK = "OK";
    private final static String PROFILE_IMAGE_ADDR = BASE + "/Photos/";
    private final static String STATUS_REQUEST = BASE + "/pairs";
    private final static String USER_INFO_ADDR = BASE + "/users/";
    private final static String MSG_POST = BASE + "/messages";
    private final static String MSG_ADDR = BASE + "/messages/";

    private Map<RequestType, String> addressMap = new HashMap<>();

    @SuppressWarnings("deprecation")
    public JSONObject makePostRequest(RequestType requestType, String json) throws JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(getAddress(requestType));
        StringEntity stringEntity = null;
        try {
            stringEntity = new StringEntity(json);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        assert stringEntity != null;
        switch (requestType) {
            case REGISTER:
                stringEntity.setContentType(CONTENT_TYPE_JSON);
                stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON));
                break;
            case LOGIN:
                stringEntity.setContentType(CONTENT_TYPE_FORM_URLENCODED);
                stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_FORM_URLENCODED));
                break;
            case SEARCH_REQUEST:
                break;
        }
        httpPost.setEntity(stringEntity);

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String response = null;
        try {
            assert httpResponse != null;
            response = EntityUtils.toString(httpResponse.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert response != null;
        if (response.isEmpty()) {
            return new JSONObject();
        }
        return new JSONObject(response);
    }

    public String makePostRequestWithToken(RequestType requestType, String json, String authToken) throws JSONException, IOException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(getAddress(requestType));
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        StringEntity stringEntity = null;
        try {
            stringEntity = new StringEntity(json);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        assert stringEntity != null;
        switch (requestType) {
            case SET_STATUS_REQUEST:
            case SET_LOCATION:
            case POST_MESSAGE:
                stringEntity.setContentType(CONTENT_TYPE_JSON);
                stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON));
                break;

        }

        httpPost.setEntity(stringEntity);
        System.out.println(httpPost);
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert httpResponse != null;
        if (httpResponse.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
            String response = EntityUtils.toString(httpResponse.getEntity());
            return new JSONObject(response).toString();
        }

        return STATUS_OK;
    }

    public String makePutRequest(RequestType requestType, String json, String authToken) throws IOException, JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPut httpPut = new HttpPut((getAddress(requestType)));
        httpPut.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        StringEntity stringEntity = new StringEntity(json);

        stringEntity.setContentType(CONTENT_TYPE_JSON);
        stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON));
        httpPut.setEntity(stringEntity);

        HttpResponse httpResponse = httpClient.execute(httpPut);
        if (httpResponse.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_OK) {
            String response = EntityUtils.toString(httpResponse.getEntity());
            return new JSONObject(response).toString();
        }
        return STATUS_OK;
    }

    public JSONObject makeGetRequest(RequestType requestType, String authToken) throws IOException, JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet((getAddress(requestType)));
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_JSON);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        String response = EntityUtils.toString(httpResponse.getEntity());
        return new JSONObject(response);
    }

    public JSONArray makeGetRequestAsJsonArray(RequestType requestType, String authToken) throws IOException, JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet((getAddress(requestType)));
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_JSON);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        String response = EntityUtils.toString(httpResponse.getEntity());
        return new JSONArray(response);
    }

    public JSONArray makeGetRequestWithJsonArray(RequestType requestType, String authToken) throws IOException, JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet((getAddress(requestType)));
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_JSON);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        String response = EntityUtils.toString(httpResponse.getEntity());
        return new JSONArray(response);
    }

    public JSONArray makeGetRequestWithIdAsJsonArray(RequestType requestType, String userId, String authToken) throws IOException, JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet((getAddress(requestType)) + userId);
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_JSON);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        String response = EntityUtils.toString(httpResponse.getEntity());
        return new JSONArray(response);
    }

    private String getAddress(RequestType requestType) {
        return getAddressMap().get(requestType);
    }

    public JSONObject makeGetRequestWithId(RequestType requestType, String userId, String authToken) throws IOException, JSONException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet((getAddress(requestType) + userId));
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_JSON);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        String response = EntityUtils.toString(httpResponse.getEntity());
        return new JSONObject(response);
    }
    public Drawable getPhoto(int idOfPhoto) {
        try {
            InputStream is = (InputStream) new URL(getAddress(RequestType.PROFILE_IMAGR_REQUEST) + idOfPhoto).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    private Map<RequestType, String> getAddressMap() {
        if (addressMap.isEmpty()) {
            initializeAddressMap();
        }
        return addressMap;
    }

    private void initializeAddressMap() {
        addressMap.put(RequestType.REGISTER, REG_ADDR);
        addressMap.put(RequestType.SEARCH_REQUEST, SEARCH_ADDR);
        addressMap.put(RequestType.LOGIN, LOGIN_ADDR);
        addressMap.put(RequestType.PROFILE_REQUEST, PROFIL_ADDR);
        addressMap.put(RequestType.PROFILE_IMAGR_REQUEST, PROFILE_IMAGE_ADDR);
        addressMap.put(RequestType.SET_LOCATION, LOCATION);
        addressMap.put(RequestType.GET_USERS, USERS_ADDR);
        addressMap.put(RequestType.ALL_PAIRS_REQUEST, PAIRS_ADDR);
        addressMap.put(RequestType.SET_STATUS_REQUEST, STATUS_REQUEST);
        addressMap.put(RequestType.USER_INFO, USER_INFO_ADDR);
        addressMap.put(RequestType.GET_MESSAGES, MSG_ADDR);
        addressMap.put(RequestType.POST_MESSAGE, MSG_POST);
    }

}
