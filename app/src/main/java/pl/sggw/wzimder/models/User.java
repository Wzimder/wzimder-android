package pl.sggw.wzimder.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mkowal on 1/6/16.
 */
@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @JsonProperty("Id")
    private String id;

    @JsonProperty("FirstName")
    private String firstName;

    @JsonProperty("Birthdate")
    private String birthdate;

    @JsonProperty("Photo")
    private int photoId;

    public User() {
    }

    public User(String id, String firstName, String birthdate, int photoId) {
        this.id = id;
        this.firstName = firstName;
        this.birthdate = birthdate;
        this.photoId = photoId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
