package pl.sggw.wzimder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

/**
 * Created by monika1212d
 */
public class LoginActivity extends AppCompatActivity {
    private final static BackendRepository backendRepository = new BackendRepository();
    private final static String PASSWORD = "password";
    private final static String EMPTYSTRING = "";
    private final static String EQUALS = "=";
    private final static String AND = "&";
    private final static String GRANTTYPE = "grant_type";
    private final static String USERNAME = "username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_register) {
            regProfile();
            return true;
        }

        if (id == R.id.action_close) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finishAffinity();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void logIn(View view) {
        new httpTask().execute(getEncodedParameters());
    }

    private String getEncodedParameters() {
        StringBuilder sb = new StringBuilder();
        try {
            EditText loginField = (EditText) findViewById(R.id.loginField);
            String loginInputText = loginField.getText().toString();
            EditText passwordField = (EditText) findViewById(R.id.passwordField);
            String loginPasswordText = passwordField.getText().toString();
            sb.append(GRANTTYPE);
            sb.append(EQUALS);
            sb.append(PASSWORD);
            sb.append(AND);
            sb.append(USERNAME);
            sb.append(EQUALS);
            String encodedLogin = URLEncoder.encode(loginInputText, "UTF-8");
            sb.append(encodedLogin);
            sb.append(AND);
            sb.append(PASSWORD);
            sb.append(EQUALS);
            sb.append(loginPasswordText);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private class httpTask extends AsyncTask<String, Integer, String> {
        private final BackendRepository backendRepository = new BackendRepository();

        protected String doInBackground(String... params) {
            String response = EMPTYSTRING;
            try {
                response = backendRepository.makePostRequest(RequestType.LOGIN, params[0]).toString();
            } catch (JSONException e) {
                System.out.println("Exception on post request.\n" + e);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            checkLoginAttempt(result);
        }
    }

    private void checkLoginAttempt(String response) {
        System.out.println("Sent login request: \n" + getEncodedParameters());
        System.out.println("Server response: \n" + response);

        if (response.contains("error"))
        {
            EditText loginField = (EditText) findViewById(R.id.loginField);
            loginField.setBackgroundColor(Color.parseColor("#DC6464"));
            EditText passwordField = (EditText) findViewById(R.id.passwordField);
            passwordField.setBackgroundColor(Color.parseColor("#DC6464"));
        }
        else
        {
            JSONObject responsejson = null;
            try {
                responsejson = new JSONObject(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (responsejson != null) {
                SharedPreferences loginPreferences = getSharedPreferences("loginPreferences",Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginPreferences.edit();
                editor.putString("userName", responsejson.optString("userName"));
                editor.putInt("expires", responsejson.optInt("expires_in"));
                editor.putString("tokenType",responsejson.optString("token_type"));
                editor.putString("issueDate",responsejson.optString(".issued"));
                editor.putString("accessToken",responsejson.optString("access_token"));
                editor.putString("expirationDate",responsejson.optString(".expires"));
                editor.commit();

                Intent intent = new Intent(LoginActivity.this, FirstActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void regProfile() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
