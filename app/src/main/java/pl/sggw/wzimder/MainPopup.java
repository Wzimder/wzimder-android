package pl.sggw.wzimder;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import pl.sggw.wzimder.fragments.MainSiteFragment;
import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

/**
 * Created by Małgosia on 30.12.2015.
 */

public class MainPopup extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_popup_window);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int popupWidth = metrics.widthPixels;
        int popupHeight = metrics.heightPixels;

        getWindow().setLayout((int) (popupWidth * .8), (int) (popupHeight * .5));

        ////add comment to popup
        final BackendRepository backendRepository = new BackendRepository();
        MainSiteFragment mainSiteFragment = new MainSiteFragment();

        TextView nameView = (TextView) findViewById(R.id.main_popup_nameView);
        TextView lastNameView = (TextView) findViewById(R.id.main_popup_lastnameView);
        TextView genderView = (TextView) findViewById(R.id.main_popup_genderView);
        TextView birthdayView = (TextView) findViewById(R.id.main_popup_birthdayView);
        TextView interestetsView = (TextView) findViewById(R.id.main_popup_interestetsView);

        String firstNameResponse = "";
        String lastNameResponse = "";
        int genderResponse = 2;
        String correctGenderResponse="nieokreślony wiek";
        String descriptionResponse = "";
        String idResponse = "";
        String birthdayResponse = "";
        int photoIdResponse = 0;

        JSONObject response = new JSONObject();
        JSONObject shortResponse = new JSONObject();
        try {
            //tu zmiana metody mmusi by pobrać dokładne dane
           /* response = backendRepository.makePostRequest(
                    RequestType.REGISTER, RegisterActivity.json);*/

            /*
            "LastName": "sample string 1",
                    "Gender": 0,
                    "Description": "sample string 2",
                    "Id": "sample string 3",
                    "FirstName": "sample string 4",
                    "Birthdate": "2016-01-06T12:49:43.8683495+01:00",
                    "Photo": 5
        */
            shortResponse = response.getJSONObject("ModelState");
            if (shortResponse.has("model.Birthdate"))
                birthdayResponse = shortResponse.getString("model.Birthdate");
            if (shortResponse.has("model.Gender"))
                genderResponse = Integer.parseInt(shortResponse.getString("model.Gender").toString());
            if (shortResponse.has("model.LastName"))
                lastNameResponse = shortResponse.getString("model.LastName");
            if (shortResponse.has("model.FirstName"))
                firstNameResponse = shortResponse.getString("model.FirstName");
            if (shortResponse.has("model.Description"))
                descriptionResponse = shortResponse.getString("model.Description");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        nameView.setText(firstNameResponse);
        lastNameView.setText(lastNameResponse);
        birthdayView.setText(birthdayResponse);
        switch (genderResponse) {
            case 1:
                correctGenderResponse="kobieta";
                break;
            case 0:
                correctGenderResponse="mężczyzna";
                break;
            default:
                correctGenderResponse = "nieokreślony wiek";
        }
        genderView.setText(correctGenderResponse);
        interestetsView.setText(birthdayResponse);

    }

}
