package pl.sggw.wzimder;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import org.json.JSONException;
import org.json.JSONObject;

import pl.sggw.wzimder.resource.BackendRepository;
import pl.sggw.wzimder.resource.RequestType;

public class RegisterActivity extends AppCompatActivity {

    private String gender = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_register);
        toolbar.setTitle(R.string.reg_activity);
        setSupportActionBar(toolbar);

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final BackendRepository backendRepository = new BackendRepository();
        final Button button = (Button) findViewById(R.id.register_button);

        button.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        try {
                            String requestJson = getJSONdata();
                            String responseJson = makeRegisterRequest(requestJson);

                            alertBuilder.setMessage(responseJson);
                            AlertDialog alertDialog = alertBuilder.create();
                            alertDialog.show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @SuppressWarnings("UnusedAssignment")
                    private String makeRegisterRequest(String requestJson) {
                        String birthdayResponse = "";
                        String emailResponse = "";
                        String passwordResponse = "";
                        String firstNameResponse = "";
                        String descriptionResponse = "";
                        String oneResponse = "";
                        String confirmPassword = "";
                        String errorResponse = "Zarejestrowano";
                        JSONObject response = new JSONObject();
                        JSONObject shortResponse = new JSONObject();
                        StringBuffer stringBuffer = null;
                        int stringBufferBeginSize = 0;

                        try {

                            response = backendRepository.makePostRequest(
                                    RequestType.REGISTER, requestJson);
                            System.out.println("Resonse \n" + response);
                            if (response.length() == 0) {
                                return errorResponse;
                            }
                            shortResponse = response.getJSONObject("ModelState");
                            System.out.println("Short response \n" + shortResponse);


                            stringBuffer = new StringBuffer("Błąd podczas rejestracji: \n");
                            stringBufferBeginSize = stringBuffer.length();

                            if (shortResponse.has("model.Birthdate")) {
                                birthdayResponse = shortResponse.getString("model.Birthdate").replace("[", "").replace("]", "");
                                stringBuffer.append(birthdayResponse).append("\n");
                            }
                            if (shortResponse.has("model.Email")) {
                                emailResponse = shortResponse.getString("model.Email").replace("[", "").replace("]", "");
                                stringBuffer.append(emailResponse).append("\n");
                            }
                            if (shortResponse.has("model.Password")) {
                                passwordResponse = shortResponse.getString("model.Password").replace("[", "").replace("]", "");
                                stringBuffer.append(passwordResponse).append("\n");
                            }
                            if (shortResponse.has("model.FirstName")) {
                                firstNameResponse = shortResponse.getString("model.FirstName").replace("[", "").replace("]", "");
                                stringBuffer.append(firstNameResponse).append("\n");
                            }
                            if (shortResponse.has("model.Description")) {
                                descriptionResponse = shortResponse.getString("model.Description").replace("[", "").replace("]", "");
                                stringBuffer.append(descriptionResponse).append("\n");
                            }
                            if (shortResponse.has("1")) {
                                oneResponse = shortResponse.getString("1").replace("[", "").replace("]", "");
                                stringBuffer.append(oneResponse).append("\n");
                            }
                            if (shortResponse.has("model.ConfirmPassword")) {
                                confirmPassword = shortResponse.getString("model.ConfirmPassword").replace("[", "").replace("]", "");
                                stringBuffer.append(confirmPassword).append("\n");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                        return (stringBuffer != null ? stringBuffer.length() : 0) > stringBufferBeginSize ? stringBuffer.toString() : errorResponse;
                    }

                }

        );
    }


    public String onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_female:
                if (checked)
                    gender = "1";
                break;
            case R.id.radio_male:
                if (checked)
                    gender = "0";
                break;
        }
        return gender;
    }


    private String getJSONdata() throws JSONException {
        JSONObject json = new JSONObject();

        EditText edit_name = (EditText) findViewById(R.id.edit_name);
        String nameToMessage = edit_name.getText().toString();

        EditText edit_surname = (EditText) findViewById(R.id.edit_surname);
        String edit_surnameToMessage = edit_surname.getText().toString();

        EditText edit_email = (EditText) findViewById(R.id.edit_email);
        String edit_emailToMessage = edit_email.getText().toString();

        EditText edit_login = (EditText) findViewById(R.id.edit_login);
        String edit_loginToMessage = edit_login.getText().toString();

        EditText edit_password = (EditText) findViewById(R.id.edit_password);
        String edit_passwordToMessage = edit_password.getText().toString();

        EditText edit_password_again = (EditText) findViewById(R.id.edit_password_again);
        String edit_password_againToMessage = edit_password_again.getText().toString();

        EditText edit_interestets = (EditText) findViewById(R.id.edit_interestets);
        String edit_interestetsToMessage = edit_interestets.getText().toString();

        EditText edit_birthdate = (EditText) findViewById(R.id.edit_birthdate);
        String edit_birthdateToMessage = edit_birthdate.getText().toString();


        json.put("FirstName", nameToMessage);
        json.put("Email", edit_emailToMessage);
        json.put("Password", edit_passwordToMessage);
        json.put("ConfirmPassword", edit_password_againToMessage);
        json.put("LastName", edit_surnameToMessage);
        json.put("Gender", gender);

        json.put("Birthdate", edit_birthdateToMessage);
        json.put("Description", edit_interestetsToMessage);
        return json.toString();
    }

}
