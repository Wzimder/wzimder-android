##### Środowisko deweloperskie:
*  [Android Studio](http://developer.android.com/tools/studio/index.html) (moim zdaniem najlepsze IDE, oczywiście jak ktoś woli Eclipse, albo Notepad++ to śmiało)
*  [Java JDK 1.7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
*  [Git](https://git-scm.com/downloads)


##### Dev
*   [dokumentacja android api](http://developer.android.com/reference/org/w3c/dom/Document.html)
*   [tutorial Darek Banas](https://www.youtube.com/watch?v=Z149x12sXsw&index=3&list=PLGLfVvz_LVvQUjiCc8lUT9aO0GsWA4uNe)
*   [backend rest api](http://52.30.121.166/Backend/Help)


##### Informacje o aplikacji:
*  Android 4.4 KitKat
*  minSdkVersion 19

##### Grafika:
*  [psd](https://onedrive.live.com/?cid=e2a504e81f4f6997&id=E2A504E81F4F6997%212597&authkey=%21AO5Ox8txtqXm5hA)
*  [png](https://drive.google.com/folderview?id=0B_TG6PAMjMHnb0N3Y2NmR1pzS3M&usp=sharing&tid=0B_TG6PAMjMHnTDBET01kWjBWc00)
*  [logo](https://www.dropbox.com/sh/v0q8tittwnl9k1d/AACdokborU0s2say5gcLHcQ6a?dl=0)

##### Strona informacyjna:
*  [link](http://wzimder.azurewebsites.net/) - **specyfikacja**, aktualności, funkcje, linki

##### Inne linki:
*  [grupy](https://onedrive.live.com/view.aspx?resid=18893B3FAC02A106!95285&ithint=file%2cxlsx&app=Excel&authkey=!AIM3dFAAty5Dpvo)
*  [notatki projektu](https://onedrive.live.com/view.aspx?resid=7FCFE85ABBB1FA83!2603&ithint=onenote%2c&app=OneNote&authkey=!ALeXo0WKBJ6uaCk)
*  [assana](https://assana.com/)

##### Inne repozytoria:
*  [backend](https://bitbucket.org/Wzimder/wzimder-backend)
*  [web](https://bitbucket.org/Wzimder/wzimder-web)
*  [win phone](https://bitbucket.org/Wzimder/wzimder-windows-phone)

##### Git:
*  [try git online](https://try.github.io/)
*  [wprowadzenie po PL](https://git-scm.com/book/pl/v1/Pierwsze-kroki-Wprowadzenie-do-kontroli-wersji)
*  [video wprowadzenie](https://git-scm.com/videos)
*  [SourceTree](https://www.sourcetreeapp.com/)

##### Tips:
*  import projektu w AndroidStudio: *File* -> *New* -> *Project from Version Control* -> *Git* -> https://mateuszkowal@bitbucket.org/Wzimder/wzimder-android.git
*  na repozytorium wrzucamy tylko działającą [!] wersję - korzystajmy z dobrodziejstw rozproszonego systemu jakim jest GIT, takich jak commit (zmiany lokalne) - push (zmiana wrzucona na serwer) po upewnieniu się że aplikacja działa
*  AndroidStudio posada wbudowaną integrację z Gitem, patrz: *VCS* -> *Update/Commit/Push*, Alt+9 pokazuje *Local Changes/Log* z repozytorium (wcześniej trzeba dać zintegrować projekt: *VCS* -> *Integrate Project*)
*  [AndroidStudio Tips and Tricks](http://developer.android.com/sdk/installing/studio-tips.html) - moim zdaniem jedne z ważniejszych to ctrl+q, ctrl+n, ctrl+shift+n
*  [Including Issues in a Commit Message](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html)
*  [Ustawianie geolokalizacji](http://developer.android.com/guide/topics/location/strategies.html#MockData) Aby "znaleźć" kogoś trzeba mieć ustawioną geolokalizację - na telefonach nie ma z tym problemu, ale na emulatorach u nas trzeba "strzelić" lokalizacją
to jest wchodzicie w Tools > Android > Android Device Monitor > Emulator Control > Location Controls > Manual > Send 


albo: 


```
#!java
telnet localhost <console-port> //(u mnie 5554)
geo fix -121.45356 46.51119 4392
geo fix 22.979593 52.222795 4392
```



*  Instalka .apk: zakładka Gradle > wzimder-android > assemble > Run